import { g, h, x } from "./xeact.js";

(async () => {
	const resp = await fetch("http://localhost:8080/api/v1/books");
	//console.log("resp", resp);
	const data = await resp.json();
	//console.log("data", data);
	const books = data.books
	//console.log("books", books);

	let booklist = g("book-list-root");
	x(booklist);

	booklist.append(...Object.entries(data.books)
		.map(b => 
			h("div", {className: `book-${b[0]}`}, [
				h("div", {className: `book-details-${b[0]}`}, [
					h("span", {className: "book-isbn", innerText: `ISBN: ${b[1].ISBN}`}),
					h("br", {}),
					h("span", {className: "book-title", innerText: `Title: ${b[1].Title}`}),
					h("br", {}),
					h("span", {className: "book-pages", innerText: `Pages: ${b[1].Pages}`}),
					h("br", {}),
				]),
				h("div", {className: `author-details-${b[0]}`}, [
					h("span", {className: "author-intro", innerText: `Writen By:`}),
					h("br", {}),
					h("span", {className: "author-fullname", innerText: `Author: ${b[1].Author.FirstName} ${b[1].Author.MiddleName} ${b[1].Author.LastName}`}),
					h("br", {}),
					h("span", {className: "author-bookswritten", innerText: `Books Written: ${b[1].Author.BooksWritten}`}),
					h("br", {}),
				]),
				h("div", {className: `publisher-details-${b[0]}`}, [
					h("span", {className: "publisher-intro", innerText: `Published By:`}),
					h("br", {}),
					h("span", {className: "publisher-name", innerText: `Publisher: ${b[1].Publisher.Name}`}),
					h("br", {}),
					h("span", {className: "publisher-bookspublished", innerText: `Books Published: ${b[1].Publisher.BooksPublished}`}),
					h("br", {}),
					h("hr", {})
				])
			])
		)
	)
})();
