# simple-xeact-boi

Like [simple-react-boi](https://gitlab.com/insanitywholesale/simple-react-boi) but in Xeact

# legal
the file `xeact.js` is vendored from the original project located
[here](https://github.com/Xe/Xeact/)
and is subject to its own license (MIT) located
[here](https://github.com/Xe/Xeact/blob/main/LICENSE)
